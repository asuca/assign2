package assign2;

public abstract class Marks {
	abstract double getPercentage();
}

class StudentA extends Marks{
	//INIT
	private int math;
	private int english;
	private int history;
	//constructor
	StudentA(int math,int english,int history){
		this.math = math;
		this.english = english;
		this.history = history;
	}
	//method
	public double getPercentage() {
		return  (double)(math + english + history) / 3;
	}
}

class StudentB extends Marks{
	//INIT
	private int math;
	private int english;
	private int history;
	private int geography;
	//constructor
	StudentB(int math,int english,int history,int geography){
		this.math = math;
		this.english = english;
		this.history = history;
		this.geography = geography;
	}
	//method
	public double getPercentage() {
		return (double)(math + english + history + geography) / 4;
	}
}
