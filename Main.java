package assign2;

public class Main {
	public static void main(String[] args) {
		//Shape -> Square
		System.out.printf("------------ \n"
				+ "-- Square -- \n------------ \n");	
		Square square = new Square(10); 
		System.out.println("The area of "
				+ "square is : " + square.getArea());
		System.out.println("The perimeter of "
				+ "square is : " + square.getPerimeter());
		//Shape -> Circle
		Circle circle = new Circle();
		circle.setLen(16);
		System.out.println("The area of "
				+ "circle is : " + circle.getArea());
		System.out.println("The perimeter of "
				+ "circle is : " + circle.getPerimeter());
		//Animals
		System.out.printf("------------ \n"
				+ "-- Animals -- \n------------ \n");
		Cat cat = new Cat();
		System.out.println("The sounds of cat is : " + cat.sound());
		Dog dog = new Dog();
		System.out.println("The sounds of dog is : " + dog.sound());
		//Marks
		System.out.printf("------------ \n"
					+ "-- Marks -- \n------------ \n");
		StudentA stuA = new StudentA(75,80,95);
		System.out.printf("StudentA's "
				+ "percentage is %.2f%%. \n",stuA.getPercentage());	
		StudentB stuB = new StudentB(75,85,85,90);
		System.out.printf("StudentB's "
				+ "percentage is %.2f%%. \n",stuB.getPercentage());
	}
}
