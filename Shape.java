package assign2;

public abstract class Shape {	
	abstract double getArea();
	abstract double getPerimeter();
}

class Square extends Shape{
	private int length;
	Square(int length){
		this.length = length;
	}
	@Override
	double getArea() {
		// TODO Auto-generated method stub
		return length * length;
	}

	@Override
	double getPerimeter() {
		// TODO Auto-generated method stub
		return length * 4;
	}	
}

class Circle extends Shape{
	private int length; //diameter
	void setLen(int length) {
		this.length = length;
	}
	double getArea() {
		return 3.14 * Math.pow((length/2),2);
	}
	double getPerimeter() {
		return 3.14 * length;
	}
}


